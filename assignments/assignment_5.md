# Assignment #5

Learn git from: https://git-scm.com/docs/gittutorial

Before beginning with the assignment make sure you registered on http://gitlab.com/

If you have not registered yet, please create an account on http://gitlab.com/


1. Fork this repository on gitlab account: https://gitlab.com/symb-assessment/startbootstrap-bare
2. Add tag to the repository as version *v0.0.1*
3. Create new branch 'media_uplaod' and checkout it.
4. Add a folder 'media'
5. Add 5 image files to 'media' folder with random names
6. Commit the changes.
7. Create new branch 'content' from master and checkout it. 
8. Add a file 'media.html' with same content as index.html
9. Commit the changes 
10. Create merge request to merge 'media' to 'master'. And merge it.
11. Tag master as version 'v0.0.2'
12. Go again to 'content' branch, and add a new file 'portfolio.html' with following content
    ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <title>Porfolio</title>
      <script type="text/javascript">
        function add(a,b){
          return a+b;
        }
      </script>
    </head>
      <body>
        <h1>Portfolio</h1>
      </body>
    </html>
    ```
13. Go again to 'media' branch and 2 more files to the 'media' folder
14. Create merge request to merge 'media' into 'master' and merge it.
15. Tag master as version 'v0.0.3'
16. Create merge request to merge 'content' into master and merge it.
18. Tag master as version 'v0.0.4'
19. Push all the changes to the remote repository

